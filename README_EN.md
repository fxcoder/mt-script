This file is mainly translated using automatic translators. If you find a mistake, report it using the [Issues](https://gitlab.com/fxcoder/mt-script/issues).

**2020-03-01: Scripts are now transferred to separate projects in the [FXcoder MQL Scripts](https://gitlab.com/fxcoder-mql) group.**
